cmake_minimum_required(VERSION 3.6)

set(CMAKE_SYSTEM_NAME Generic)

# Avoid running the linker since we need flags for this, so try_compile fails.
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_SYSTEM_PROCESSOR "arm")
set(CROSS_COMPILE arm-none-eabi-)

if(NOT TC_PATH)
    set(TC_PATH /usr)
endif()

set(CMAKE_FIND_ROOT_PATH ${CMAKE_SOURCE_DIR})

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(CMAKE_C_COMPILER   ${TC_PATH}/bin/arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER ${TC_PATH}/bin/arm-none-eabi-g++)

# Specify our C library as the system root for this tool-chain
if(NOT CMAKE_SYSROOT)
    set(NEWLIB_DIR ${CMAKE_CURRENT_LIST_DIR}/../libs/newlib/arm-none-eabi/)
    set(CMAKE_SYSROOT ${NEWLIB_DIR})
    set(CMAKE_EXE_LINKER_FLAGS -L${NEWLIB_DIR}/lib ${CMAKE_EXE_LINKER_FLAGS})
endif()

# Make sure we don't have arm-none-eabi-newlib installed as a system package as this will
# cause conflicts with our self-built newlib found in libs/newlib.
if(EXISTS "/usr/arm-none-eabi/include/sys/_types.h")
    message(FATAL_ERROR "sys/_types.h from arm-none-eabi-newlib package found. "
        "Please remove this package as it will conflict with libs/newlib")
endif()

message(STATUS "SYSROOT set to ${CMAKE_SYSROOT}")
