# add googletest from the given folder, but only if the target does not already
# exist.
function(add_googletest folder)
    if(NOT TARGET gtest AND NOT TARGET gmock AND NOT TARGET gtest-main AND NOT TARGET gmock-main)
        add_subdirectory("${folder}")
        
        # please note that gtest and gmock turn on -Werror and with -Wextra
        # enabled, throws some unused parameter warnings causing the build to 
        # fail. We turn off these warnings for these targets.
        target_compile_options(gtest PRIVATE -Wno-unused-parameter)
        target_compile_options(gmock PRIVATE -Wno-unused-parameter)
    endif()
endfunction()
