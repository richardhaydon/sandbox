# add rapidcheck from the given folder, but only if the target does not already
# exist.
function(add_rapidcheck folder)
    message(info "in the add rapidcheck folder cmake function")
    if(NOT TARGET rapidcheck AND NOT TARGET rapidcheck-gtest AND NOT TARGET rapidcheck-gmock)
        # we would like gtest and gmock support
        option(RC_ENABLE_GTEST "Forcing Rapidcheck GTest support" ON)
        option(RC_ENABLE_GMOCK "Forcing Rapidcheck GMock support" ON)

        message(info "adding the folder " ${folder} " for rapidcheck")
        add_subdirectory("${folder}")

        # please note that rapidcheck turns on -Werror and with -Wextra enabled
        # throws some unused parameter and negative left shift warnings, causing
        # the build to fail. We turn off these warnings for this target.
        target_compile_options(rapidcheck PRIVATE -Wno-unused-parameter -Wno-shift-negative-value)
    endif()
endfunction()
