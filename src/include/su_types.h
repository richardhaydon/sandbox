#pragma once
#include <string>

/// WideNorth demo project definitions of the values exchanged in the defined
/// signaling units of the example calling protocol

/**
 * @brief The service_t enum some service types that can be used in the
 * following su definitions
 */
enum class service_t { pots, pck_data, alert };

/**
 * The address the lower 24 bits of a 32 bit register, the upper 8 bits are
 * reserved and always 0's
 */
using network_addr = uint32_t;

const network_addr addr_mask = 0x00FFFFFF;
const network_addr broadcast_addr = 0xFF000000;

/**
 * Tx level backoff, we use an int and name it to be more expessive in the code
 */
using backoff = int;

/**
 * @brief The rejection_condition_t enum codes in the access denied su
 */
enum class rejection_condition_t {
  network_busy,
  destination_unknown,
  destination_busy
};

/**
 * The channel assigned for communication in the access granted su
 */
using channel_number = uint8_t;
