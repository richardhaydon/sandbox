#include "node.hpp"
#include <iostream>
#include <su.hpp>

/**
 * @brief node::node constructor
 */
node::node() {}

/**
 * @brief node::run entry point
 * @return runs forever
 */
int node::run() {
  std::cout << "Entering the main method for node" << std::endl;

  return 0;
}
