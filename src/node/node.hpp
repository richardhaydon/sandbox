#ifndef NODE_HPP
#define NODE_HPP

#include <archer_defs.hpp>
#include <stdio.h>

// Node is an object that uses the su serialization library

class node {
public:
  node();
  // entry point method
  int run();
};

// our main execution thread
int main(int argc, char *argv[]) {
  // start a single instance of a node
  node n;
  printf("Starting a node \n");
  return n.run();
}

#endif // NODE_HPP
