# make a library exporting our wrapper around the su serialization

set (su_lib_src
    ${CMAKE_CURRENT_SOURCE_DIR}/su.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/su.cpp
    )
add_library(su_lib ${su_lib_src})
target_include_directories(su_lib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/cereal/include)
