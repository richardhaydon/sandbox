#pragma once

#include <archer_defs.hpp>

#include <su_types.h>

/// pull in the headers of the cereal serialization library
#include <cereal/archives/json.hpp> // for the archive type we will be using
#include <cereal/types/memory.hpp>
#include <cereal/types/unordered_map.hpp>
#include <fstream>
#include <memory>

/**
 * @brief The su struct base type for all signalling units in the WideNorth
 * demo project.
 */
struct su {
  /**
   * Serialization requires a default constrtore
   */
  su() = default;
  /**
   * Su constructor
   * @brief su
   * @param su_number
   * @param origin
   * @param destination
   */
  su(uint8_t su_number, network_addr origin, network_addr destination)
      : m_su_number(su_number), m_origin(origin), m_destination(destination){};

  virtual ~su() = default;

  virtual bool operator==(const su &other) {
    return this->m_su_number == other.m_su_number &&
           this->m_destination == other.m_destination &&
           this->m_origin == other.m_origin;
  }

  /// common payload for all su specializations
  uint8_t m_su_number;
  network_addr m_origin;
  network_addr m_destination;

  template <class Archive> void serialize(Archive &ar) {
    ar(CEREAL_NVP(m_origin), CEREAL_NVP(m_destination),
       CEREAL_NVP(m_su_number));
  }
};

/**
 * @brief Access request SU constructor
 */
struct access_request : public su {
public:
  service_t m_service;

  template <class Archive> void serialize(Archive &ar) {
    ar(cereal::base_class<su>(this), CEREAL_NVP(m_service));
  }
};

/**
 * @brief Channel assignment SU
 */
struct channel_assignment : public su {

  channel_number m_chn;

  template <class Archive> void serialize(Archive &ar) {
    ar(cereal::base_class<su>(this), CEREAL_NVP(m_chn));
  }
};

/**
 * @brief Access Reject SU
 */
struct access_reject : public su {
  rejection_condition_t m_cause;

  template <class Archive> void serialize(Archive &ar) {
    ar(cereal::base_class<su>(this), CEREAL_NVP(m_cause));
  }
};

// Allow polymorphic support by registering the types we use in the hierarchy
CEREAL_REGISTER_TYPE(su);
CEREAL_REGISTER_TYPE(access_request);
CEREAL_REGISTER_TYPE(channel_assignment);
CEREAL_REGISTER_TYPE(access_reject);
