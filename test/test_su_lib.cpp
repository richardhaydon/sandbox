#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rapidcheck/gtest.h>

#include <serialization/su.hpp>

using testing::_;
// Tell rapidcheck how to generate service types and fault codes
namespace rc {
template <> struct Arbitrary<service_t> {
  static Gen<service_t> arbitrary() {
    return gen::cast<service_t>(
        gen::inRange(0, 1 + static_cast<int>(service_t::alert)));
  }
};
template <> struct Arbitrary<rejection_condition_t> {
  static Gen<rejection_condition_t> arbitrary() {
    return gen::cast<rejection_condition_t>(gen::inRange(
        0, 1 + static_cast<int>(rejection_condition_t::destination_busy)));
  }
};

} // namespace rc

// Create a test fixture to handle common setup and teardown between the tests
class SuTestFixture : public ::testing::Test {
public:
  SuTestFixture() {}
  ~SuTestFixture() {}

  std::istringstream m_i_archive_stream;
};

// Serialize and deserialize a single SU to show the functionality to and from
// json
TEST_F(SuTestFixture, test_serialize) {
  // Simplest example base type signalling unit record
  su original_su(42, 1, 2);

  std::stringstream s_stream;
  {
    cereal::JSONOutputArchive json_archiver(s_stream);
    json_archiver(original_su);
  }
  std::cout << s_stream.str();

  // Now deserialize into an su
  su decoded_su;

  cereal::JSONInputArchive i_archive(s_stream);
  i_archive(decoded_su);
  EXPECT_TRUE(decoded_su == original_su);
}

///// An example of a static spot check type of unit test for a property
// TEST(access_request, spot_check) {
//  access_request req(0xDEADBEEF, 0xBADDCAFE, service_t::pck_data);
//  EXPECT_EQ(req.m_su_number, 1u);
//  EXPECT_EQ(req.m_origin, 0xADBEEF);
//  EXPECT_EQ(req.m_destination, 0xDDCAFE);
//  EXPECT_EQ(req.m_service, service_t::pck_data);
//}

///// Test creation of the different types of signalling unit
// RC_GTEST_PROP(access_request_constructor, properties,
//              (network_addr origin, network_addr destination,
//               service_t service)) {

//  /// create the access request with the Rapidcheck properties
//  access_request req(origin, destination, service);
//  /// assert the properites, the origin and destination are constrained to the
//  /// low 24 bits
//  RC_ASSERT(req.m_su_number == 1u);
//  RC_ASSERT(req.m_origin == (origin & 0x00FFFFFF));
//  RC_ASSERT(req.m_destination == (destination & 0x00FFFFFF));
//  RC_ASSERT(req.m_service == service);
//  // printf("origin = %x dest = %x service =%d\n", req.m_origin,
//  // req.m_destination, static_cast<int>(req.m_service));
//}

///// Test creation of the different types of signalling unit
// RC_GTEST_PROP(channel_assignment_constructor, properties,
//              (network_addr origin, network_addr destination,
//               channel_number chn)) {

//  /// create the access request with the Rapidcheck properties
//  channel_assignment assignment(origin, destination, chn);

//  RC_ASSERT(assignment.m_su_number == 2u);
//  RC_ASSERT(assignment.m_origin == (origin & 0x00FFFFFF));
//  RC_ASSERT(assignment.m_destination == (destination & 0x00FFFFFF));
//  RC_ASSERT(assignment.m_chn == chn);
//}

///// Test creation of the different types of signalling unit
// RC_GTEST_PROP(access_rejection_constructor, properties,
//              (network_addr origin, network_addr destination,
//               rejection_condition_t r)) {

//  /// create the access reject with the Rapidcheck properties
//  access_reject rejection(origin, destination, r);
//  RC_ASSERT(rejection.m_su_number == 3u);
//  RC_ASSERT(rejection.m_origin == (origin & 0x00FFFFFF));
//  RC_ASSERT(rejection.m_destination == (destination & 0x00FFFFFF));
//}
